---
title: Informationspaket
date: 2017-02-27 16:59:10
contribute_url: https://gitlab.com/swe-nrb/nrb-sbp-shopfront/blob/master/source/philosophy/information-package/index.md
is_children: true
---

Ett informationspaket är i sin enkelhet en behållare för information. Informationen är direkt läsbar men kan kan också extraheras till nya informationsmängder (t.ex. word-filer, webbsida (html), pdf). Vidare kan informationen utryckas och lagras på olika nivåer beroende på behov och komplexitet. 

> Ett informationspaket är i sin enkelhet en behållare för information

### Anatomi  

I syfte att göra det möjligt att automatisera vissa processer vid extrahering av information så finns en fastlagd struktur som ett informationspaket måste följa. Tanken är att genom att ha tydligt ramverk så kan delar i processen bytas ut efter behov utan att påverka helheten.

![Illustration över extraheringsprocessen](./grafik/process.svg)

#### Informationsnivåer (mappar)  

Nedan följer en förteckning över de mappar som är definierade i ramverket.

##### skript  

Här lagras filer som i förstegsprocessen bildar textfiler. Ramverket lämnar frihet gällande filformat och struktur här, begränsningarna kommer snarare ifrån [byggpaketens](../build-package) funktionalitet.

![Illustration över innehåll i skriptmappen](./grafik/skript.svg)

##### innehall 

Här lagras "färdiga" textfiler (`.md` eller `.html`). Dessa är antingen versionshanterade eller genererade från "försteget". Dessa filer blir input i extraheringssteget. Hur dessa filer hamnar i mappen är underordnat utan det viktiga är att dem är strukturerade på ett likartat sätt
så att dom kan jacka i en generell extraheringsprocess.

![Illustration över innehåll i innehållsmappen](./grafik/innehall.svg)

##### utdata  

Här lagras generede utdata (t.ex. word-filer eller pdf:er). Efter att extraheringsprocessen är färdig så kan dessa filer (slutresultatet) distribueras fritt.

![Illustration över innehåll i utdatamappen](./grafik/utdata.svg)

#### Processer  

För att tranformera information mellan informationsnivåer har några fasta processer definierats. Dessa processer kan automatiseras med [byggpaket](../build-package).

##### Försteg  

Info kommer inom kort.

##### Extrahering  

Info kommer inom kort.
